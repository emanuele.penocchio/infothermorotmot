# InfoThermoRotMot

This repository shares the Jupyter notebook code used to generate the plots in the article titled "Insights from an information thermodynamics analysis of a synthetic molecular motor" by Amano, Esposito, Kreidt, Leigh, Penocchio, and Roberts appeared in  Nat. Chem. 14, 530–537 (2022). https://doi.org/10.1038/s41557-022-00899-z.

### System requirements

The supplementary code is made available as a .ipynb file and uses an embedded dataset that serves as a basis to run the code.

It can be executed using Jupyter Notebook environment in any operating system.

Software dependencies are indicated:

- Python 3.6+

- numpy 1.16+

- matplotlib 3.3+

- scipy 1.4+

- IPython 7+

- Jupyter Notebook

The software has been tested on Linux (Ubuntu 20.04.3 LTS), Windows (10-pro) and macOS (High Sierra 10.13.6).
No non-standard hardware is required.
To run the code download the contents of the current repository.
The code is ready to run in the Jupyter Notebook environment provided that the above requirements are satisfied.
